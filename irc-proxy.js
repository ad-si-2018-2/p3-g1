/*

 .d8888b.                            d8b      888                       8888888 8888888b.   .d8888b.  
d88P  Y88b                           Y8P      888                         888   888   Y88b d88P  Y88b 
Y88b.                                         888                         888   888    888 888    888 
 "Y888b.    .d88b.  888d888 888  888 888  .d88888  .d88b.  888d888        888   888   d88P 888        
    "Y88b. d8P  Y8b 888P"   888  888 888 d88" 888 d88""88b 888P"          888   8888888P"  888        
      "888 88888888 888     Y88  88P 888 888  888 888  888 888            888   888 T88b   888    888 
Y88b  d88P Y8b.     888      Y8bd8P  888 Y88b 888 Y88..88P 888            888   888  T88b  Y88b  d88P 
 "Y8888P"   "Y8888  888       Y88P   888  "Y88888  "Y88P"  888          8888888 888   T88b  "Y8888P"  
 
 
                                     8888888b.           888      888     d8b  888    888b     d888  .d88888b.  
                                    888   Y88b          888      888      Y8P 888    8888b   d8888 d88P" "Y88b 
                                    888    888          888      888          888    88888b.d88888 888     888 
 .d8888b .d88b.  88888b.d88b.       888   d88P  8888b.  88888b.  88888b.  888 888888 888Y88888P888 888     888 
d88P"   d88""88b 888 "888 "88b      8888888P"      "88b 888 "88b 888 "88b 888 888    888 Y888P 888 888     888 
888     888  888 888  888  888      888 T88b   .d888888 888  888 888  888 888 888    888  Y8P  888 888 Y8b 888 
Y88b.   Y88..88P 888  888  888      888  T88b  888  888 888 d88P 888 d88P 888 Y88b.  888   "   888 Y88b.Y8b88P 
 "Y8888P "Y88P"  888  888  888      888   T88b "Y888888 88888P"  88888P"  888  "Y888 888       888  "Y888888"  
                                                                                                          Y8b  
                                                                                                               

// Sobre: Desenvolvido para disciplina de Aplicações (Projeto 3) do grupo 01;
// Data: 15/12/2018
// Disponível em: https://gitlab.com/ad-si-2018-2/p3-g1
// Feito com base no protótipo: https://gitlab.com/sd1-ec-2017-1/p3/tree/motd                                                                                                     
                                                                                                      */

var irc = require('irc');
var amqp = require('amqplib/callback_api');

var proxies = {}; // mapa de proxys
var amqp_conn;
var amqp_ch;
var irc_clients = {};
var pingRequest;

// Conexão com o servidor AMQP
amqp.connect('amqp://localhost', function(err, conn) {
	
	conn.createChannel(function(err, ch) {
		
		amqp_conn = conn;
		amqp_ch = ch;
		
		inicializar();
	});
});

function inicializar() {
	
	receberDoCliente("registro_conexao_irc", function (msg) {
		
		console.log('irc-proxy.js: recebeu registro de conexão');
		
		var id       = msg.id;
		var servidor = msg.servidor;
		var nickWeb  = msg.nick;
		var canal    = msg.canal;
		var porta    = msg.porta;
		
		let url = 'irc://' + nickWeb + '@' + servidor + ':' + porta + '/' + canal

		irc_clients[id] = new irc.Client(
			servidor, 
			nickWeb,
			{channels: [canal]}
		);		
		
		irc_clients[id].addListener('message'+canal, function (from, message) {
			
			console.log(from + ' => '+ canal +': ' + message);
			
			enviarParaCliente(url, {
				"timestamp": Date.now(), 
						   "nick": from,
				 "msg": message
			});
		});
		
		irc_clients[id].addListener('error', function(message) {
			
			enviarParaCliente(url, {
				"timestamp": Date.now(), 
				"nick": "Servidor",
				"msg": JSON.stringify(message)});
			
		});

		// Listener do comando motd;
		irc_clients[id].addListener('motd', function(message) {
			enviarParaCliente(url, {
				"timestamp": Date.now(), 
				"nick": "Servidor",
				 "msg": message
			});			
		});

		// Listener do comando nick; @jeanMatsunaga 15/12/2018 - 20:07;
		irc_clients[id].addListener('nick', function(oldNick, newNick, channels, message) 
		  {
			enviarParaCliente(url,{"timestamp": Date.now(),
								  "nick": "Servidor",
								  "msg": oldNick + " alterou o apelido para " + newNick});
		  });
		
		// Listener do comando whois; @jeanMatsunaga 15/12/2018 - 23:34;
		irc_clients[id].addListener('whois', function(whois)
		  {
		    enviarParaCliente(url,{"timestamp": Date.now(),
								  "nick": "Servidor",
								  "msg": JSON.stringify(whois)})
		  });
		
		// Listener do comando ping/pong; @jeanMatsunaga 16/12/2018 - 00:15;
		irc_clients[id].addListener('pong', function(pong)
		  {
			if (pingRequest) {
		      enviarParaCliente(url,{"timestamp": Date.now(),
				  				    "nick": "Servidor",
								    "msg": "Ping solicitado? Pong! " + pong})
			  pingRequest = false;
							 }
		  });
		
		// Listener do comando quit; @jeanMatsunaga 16/12/2018 - 00:45;
		irc_clients[id].addListener('quit', function(message)
		  {
			enviarParaCliente(url,{"timestamp": Date.now(),
								  "nick": "Servidor",
							      "msg": message})
		  });
		
		// Listener do comando topic; @jeanMatsunaga 17/12/2018 - 12:16;
		irc_clients[id].addListener('topic', function(channel, topic, createdBy, message)
		  {
			enviarParaCliente(url,{"timestamp": Date.now(),
								  "nick": "Servidor",
							      "msg": topic + " created by " + createdBy})
		  });
		  
		// Listener do comando names; @jeanMatsunaga 16/12/2018 - 01:04;
		irc_clients[id].addListener('names', function(channel, nicks)
		  {
			enviarParaCliente(url,{"timestamp": Date.now(),
								  "nick": "Servidor",
							      "msg": "Usuarios no canal " + channel + ":" + JSON.stringify(nicks)})
		  });

		// Listener do comando part; @jeanMatsunaga 16/12/2018 - 01:07;
		irc_clients[id].addListener('part', function(message)
		  {
			enviarParaCliente(url,{"timestamp": Date.now(),
								  "nick": "Servidor",
							      "msg": JSON.stringify(message)})
		  });
		  
		// Listener do comando kill; @jeanMatsunaga 17/12/2018 - 12:27;
		irc_clients[id].addListener('kill', function(nick, reason, channels, message)
		  {
			enviarParaCliente(url,{"timestamp": Date.now(),
								  "nick": "Servidor",
							      "msg":  "O usuario " + nick + " foi removido do servidor! Motivo: " + reason})
		  });
		  
		// Listener do comando kick; @jeanMatsunaga 15/12/2018 - 23:34;
		 irc_clients[id].addListener('kick', function(channel, kickedUser, kickedBy, kickReason, message) 
		  {
		    enviarParaCliente(url,{"timestamp": Date.now(),
								  "nick": "Servidor",
								  "msg": "O usuário " + kickedUser + " foi kickado do canal " + channel + ". Motivo: " + kickReason + "."})
		  }); 
		  
		proxies[id] = irc_clients[id];
	});
	
	receberDoCliente("mensagem_irc", function (msg) {
		
		// Verifica se o primeiro caractere é "/", se for interpreta como comando, else interpreta como mensagem de texto;
		if (msg.msg.charAt(0) == '/') {

		   var cmd = msg.msg.split(" ");
		// var cmd = cmdSeparado.toString().toLowerCase(); <- Pensei em remover a "/" e tratar separado mas não deu certo =/
		// var cmdSemBarra = cmd.split('/')[1];

		   // Switchcase para verificar o comando digitado;
		   switch (cmd[0].toLowerCase()) {
			   case '/motd':
			   irc_clients[msg.id].send('motd');
			   break;
			  
			   case '/nick':
	           var newNick = cmd[1]; // @autor: jeanMatsunaga;
			   irc_clients[msg.id].send('nick', newNick);
			   break;
			   
			   case '/whois': // @autor: jeanMatsunaga;
			   var whoisNick = cmd[1];
			   irc_clients[msg.id].send('whois', whoisNick);
			   break;
			   
			   case '/ping': // @autor: jeanMatsunaga;
			   var userServidor = cmd[1];
			   irc_clients[msg.id].send('ping', userServidor);
			   
			   pingRequest = true;
			   
			 /* BUGFIX: Inseri uma variavel de controle de requisição de ping pois
						o listener estava recebendo um PONG a cada 15 segundos, é
						provável que isso seja para verificar o status da conexao.
						Solucionado inserindo uma validação pingRequest. 
						@JeanMatsunaga;
			 
			   [00:19:24 - Servidor]: Ping Request? Pong! card.freenode.net
			   [00:19:39 - Servidor]: Ping Request? Pong! card.freenode.net
			   [00:19:54 - Servidor]: Ping Request? Pong! card.freenode.net
			   [00:20:09 - Servidor]: Ping Request? Pong! card.freenode.net
			   [00:20:25 - Servidor]: Ping Request? Pong! card.freenode.net
			   [00:20:40 - Servidor]: Ping Request? Pong! card.freenode.net */

			   break;
			   
			   case '/quit': // @autor: jeanMatsunaga;
			   var quitReason = cmd[1];
			   irc_clients[msg.id].send('quit',quitReason);
			   break;
			   
			  /* case '/time': // Utilizar comando /time Localhost
			   var serverTime = cmd[1];
			   irc_clients[msg.id].send('time',serverTime);
			   break; */
			   
			   case '/kill': /* Comando utilizado apenas por administradores do SERVIDOR */ // @autor: jeanMatsunaga;
			   var nick = cmd[1];
			   var reason = cmd[2];
			   var channel = cmd[3];
			   var message = cmd[4];
			   irc_clients[msg.id].send('kill',nick, reason, channel, message); 
			   break;
			  
			   
			   case '/kick': // @autor: jeanMatsunaga;
			   var channel = cmd[1]; // Correção @jeanMatsunaga, parametros estavam invertidos!
			   var kickedUser = cmd[2];
			   var kickReason = cmd[3];
			   irc_clients[msg.id].send('kick', channel, kickedUser, kickReason);
			   break;


			   case '/topic': // @autor: jeanMatsunaga;
			   var channel = cmd[1]
			   irc_clients[msg.id].send('topic',channel); // Correção @jeanMatsunaga, parametros corrigidos.
			   break;
			   
			   case '/names': // @autor: jeanMatsunaga;
			   var channelName = cmd[1];
			   irc_clients[msg.id].send('names',channelName);
			   break;
			   
			   case '/part': // @autor: jeanMatsunaga;
			   var channelPart = cmd[1];
			   irc_clients[msg.id].send('part',channelPart);
			   break;
		   
		    }
		} else 
			irc_clients[msg.id].say(msg.canal, msg.msg);
		}
	);
}

function receberDoCliente (canal, callback) {
	
	amqp_ch.assertQueue(canal, {durable: false});
	
	console.log(" [irc] Waiting for messages in "+canal);
	
	amqp_ch.consume(canal, function(msg) {
		
		console.log(" [irc] Received %s", msg.content.toString());
		callback(JSON.parse(msg.content.toString()));
		
	}, {noAck: true});
}

function enviarParaCliente (url, msg) {
	
	msg = new Buffer(JSON.stringify(msg));
	
	amqp_ch.assertQueue(url, {durable: false});
	amqp_ch.sendToQueue(url, msg);
	console.log(" [irc] Sent to queue "+url+ ": "+msg);
}