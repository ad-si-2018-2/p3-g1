# DEFINIÇÕES E TUTORIAIS DE PROJETO

Protótipo do projeto 3 das disciplinas de Aplicações Distribuídas (SI) e Sistemas Distribuídos (EC) do semestre 2017-1.
PROFESSOR: Marcelo Akira Inuzuka - akira@marceloakira.com

## MEMBROS

* [X] Marcella Sueizy de Toledo - Líder
* [X] Gabriela Nogueira Mansur - Documentadora
* [X] Jean Matsunaga - Desenvolvedor
* [X] Rodrigo Marden - Documentador
* [X] Victor Murilo - Documentador/FrontEnd

## DOCUMENTAÇÃO

- [Documentação do projeto](https://gitlab.com/ad-si-2018-2/p3-g1/wikis/home)

## RFC IRC 

- [RFC IRC 1459](https://tools.ietf.org/html/rfc1459)
- [RFC IRC 2812 CLIENT](https://tools.ietf.org/html/rfc2812)
- [RFC IRC 2813 SERVER](https://tools.ietf.org/html/rfc2813)